let initialState = {
    forecast: {
        city: "Lyon",
        date: new Date().toDateString(),
        temperature: '?',
        apiUrlBase:'http://api.weatherstack.com/current?access_key=d094f47bb20878ba8b1fdfda3c3ebc4c'
    },
    loader: false
}

export const forecastReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_CITY':
            return {
                ...state,
                city: action.value
            }
        case 'UPDATE_TEMPERATURE':
            return {
                ...state,
                city: action.value
            }
        default:    
            return state;
    }
}
