import React from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { weatherFromApi } from "../../actions/forecastAction"

class ForecastForm extends React.Component{
    render(){
        return (
            <div>
                <input type='text'></input>
                <button onClick={this.props.weatherFromApi}>Search</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        city: state.forecastReducer.forecast.city,
        date: state.forecastReducer.forecast.date
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        weatherFromApi
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm);
