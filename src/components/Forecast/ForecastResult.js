import React from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { weatherFromApi } from "../../actions/forecastAction"

class ForecastResult extends React.Component{
    render(){
        return (
            <div>
                <p>{this.props.temperature} °C</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        temperature: state.forecastReducer.forecast.temperature,
    }
}


export default connect(mapStateToProps)(ForecastResult);
