export const weatherFromApi = () =>{
    return async dispatch =>{
        const response = await fetch('http://api.weatherstack.com/current?access_key=d094f47bb20878ba8b1fdfda3c3ebc4c&query='+'lyon');
        const data = await response.json();
        console.log(data.current);
    }
}

export const updateCity = (value) =>{
    return{
        type: 'UPDATE_CITY',
        value
    }
}

export const updateTemperature = (value) =>{
    return{
        type: 'UPDATE_TEMPERATURE',
        value
    }
}